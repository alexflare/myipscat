﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MyIpCat.Model;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;

namespace MyIpCat.Controllers
{
    public class HomeController : Controller
    {
        private enum CatBreeds
        {
            emau = 1,
            abob = 2,
            norw = 3,
            mcoo = 4,
            bamb = 5,
            beng = 6,
            sphy = 7,
            munc = 8,
            siam = 9
        }

        public IActionResult Index()
        {
            var remoteIpAddress = GetIpAddressOfClient();
            ViewBag.Cat = CatMapper(GetIpCat(remoteIpAddress));

            return View();
        }

        // reference: https://www.thecodebuzz.com/get-ip-address-in-asp-net-core-guidelines/comment-page-1/?unapproved=20953&moderation-hash=9618e5c47c7c7fa36fa8655d9e3c86c9#comment-20953
        private string GetIpAddressOfClient()
        {
            string ipAddress = string.Empty;
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            if (ip == null)
            {
                return ipAddress;
            }

            if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
            {
                ip = Dns.GetHostEntry(ip).AddressList
                    .FirstOrDefault(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
            }

            ipAddress = ip.ToString();

            return ipAddress;
        }

        private string GetBreedId(string IpAddress)
        {
            string numberSequence = IpAddress.Replace(".", "");
            int number = DigitSum(int.Parse(numberSequence));
            string breed = ((CatBreeds)number).ToString();

            return breed;
        }

        // he he he he
        private int DigitSum(int num)
        {
            int sum = 0;
            while (num > 0)
            {
                sum += num % 10;
                num /= 10;
            }

            if (sum > 9)
            {
                sum = DigitSum(sum);
            }

            return sum;
        }

        private Cat GetIpCat(string IpAddress)
        {
            var client = new RestClient("https://api.thecatapi.com/v1/images/search");
            var request = new RestRequest() { Method = Method.Get };
            request.AddParameter("has_breeds", true);
            request.AddParameter("breed_ids", GetBreedId(IpAddress));
            request.AddParameter("size", "small");
            request.AddHeader("x-api-key", "apikeyhere");

            List<Cat> response = JsonSerializer.Deserialize<List<Cat>>((client.Execute(request)).Content);

            return response.FirstOrDefault();
        }

        public CatPresentation CatMapper(Cat cat)
        {
            var config = new MapperConfiguration(x =>
            {
                x.CreateMap<Cat, CatPresentation>().ConvertUsing(source => source.breeds.Select(b => new CatPresentation
                {
                    url = source.url,
                    name = b.name,
                    description = b.description,
                    wikipedia_url = b.wikipedia_url
                }).FirstOrDefault());
            });

            return config.CreateMapper().Map<CatPresentation>(cat);
        }
    }
}