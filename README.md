# My IP Cat

Small .Net Core project to practice Automapper. 

It calls the [Cat API](https://thecatapi.com) to get a cat's information based on a breed number. The project gets the breed number my adding up the visitor's ip until it's one digit. 

It has bare minimum styling so don't expect anything pretty here.

Nyaaa ~ 
