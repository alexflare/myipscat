﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyIpCat.Model
{
    public class Cat
    {
        public string id { get; set; }
        public string url { get; set; }
        public List<Breed> breeds { get; set; }
    }
}
