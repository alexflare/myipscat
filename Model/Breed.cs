﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyIpCat.Model
{
    public class Breed
    {
        public string id { get; set; }
        public string name { get; set; }
        public string temperament { get; set; }
        public string origin { get; set; }
        public string country_code { get; set; }
        public string description { get; set; }
        public string wikipedia_url { get; set; }
    }
}
