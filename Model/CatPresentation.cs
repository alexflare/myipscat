﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyIpCat.Model
{
    public class CatPresentation
    {
        public string url { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public string wikipedia_url { get; set; }
    }
}
